resource "aws_ecs_task_definition" "test" {
  family                   = "test"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  container_definitions = jsonencode([{
    name        = "petclinic"
    image       = "registry.gitlab.com/votal1/petclinic:latest"
    essential   = true
    portMappings = [{
      protocol      = "tcp"
      containerPort = 8080
      hostPort      = 8080
    }]
  }])
}

resource "aws_ecs_service" "example" {
  name = "example"
  cluster  = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.test.arn
  desired_count = 2
  launch_type = "FARGATE"
  scheduling_strategy = "REPLICA"

  network_configuration {
    security_groups = toset([aws_security_group.sg_service.id])
    subnets = toset([aws_subnet.first.id, aws_subnet.second.id])
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.test.arn
    container_name   = "petclinic"
    container_port   = 8080
  }
}

}

