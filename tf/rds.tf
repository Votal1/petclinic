resource "aws_db_subnet_group" "db_subnet_group" {
    subnet_ids  = [aws_subnet.first_private.id, aws_subnet.second_private.id]
}

resource "aws_db_instance" "rds" {
  allocated_storage      = 5
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  db_name                = "petclinic"
  username               = "petclinic"
  password               = "petclinic"
  port                   = 3306
  db_subnet_group_name   = aws_db_subnet_group.db_subnet_group.id
  vpc_security_group_ids = [aws_security_group.sg_rds.id]
  skip_final_snapshot    = true
  multi_az               = true
}
