resource "aws_ecs_cluster" "cluster" {
  name = "petclinic"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_task_definition" "test" {
  family                   = "test"
  requires_compatibilities = ["EC2"]
  network_mode             = "bridge"
  cpu                      = 128
  memory                   = 256
  container_definitions = jsonencode([{
    name        = "petclinic"
    image       = "registry.gitlab.com/votal1/petclinic:latest"
    environment = [{"MYSQL_HOST": "${aws_db_instance.rds.endpoint}"}]
    essential   = true
    portMappings = [{
      protocol      = "tcp"
      containerPort = 8080
      hostPort      = 8080
    }]
  }])
  depends_on = [aws_db_instance.rds]
}

resource "aws_ecs_service" "example" {
  name = "example"
  cluster  = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.test.arn
  desired_count = 2
  launch_type = "EC2"
  scheduling_strategy = "REPLICA"

  load_balancer {
    target_group_arn = aws_lb_target_group.test.arn
    container_name   = "petclinic"
    container_port   = 8080
  }
}

