resource "aws_launch_configuration" "ecs_launch_config" {
    image_id             = "ami-0f863d7367abe5d6f"
    # associate_public_ip_address = true
    iam_instance_profile = aws_iam_instance_profile.ecs_agent.name
    security_groups      = [aws_security_group.sg_service.id]
    user_data            = "#!/bin/bash\necho ECS_CLUSTER=petclinic >> /etc/ecs/ecs.config;echo ECS_BACKEND_HOST= >> /etc/ecs/ecs.config;"
    instance_type        = "t2.micro"
}

resource "aws_autoscaling_group" "failure_analysis_ecs_asg" {
    name                      = "asg"
    vpc_zone_identifier       = [aws_subnet.first_private.id, aws_subnet.second_private.id]
    launch_configuration      = aws_launch_configuration.ecs_launch_config.name

    desired_capacity          = 2
    min_size                  = 2
    max_size                  = 4
    health_check_grace_period = 300
    health_check_type         = "EC2"
}
