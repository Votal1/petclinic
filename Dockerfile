FROM openjdk:11
VOLUME /tmp
ENV spring.profiles.active=mysql
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
